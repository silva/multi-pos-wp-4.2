%%%
%  ONGOING SCRIPT
%
%  This script retrieves the CF in the provided SCF (N runs) and applies
%  the normaliser method
%
%   REFERENCE
%   A Segmentation Technique Based on Standard Deviation in Body Sensor
%   Networks, http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=4454174&tag=1
%
%%% IMPORTANT
%   Energy vector is organised as follows
%       Energy = [ IDX: CF from SCF(1): ... : CF from SCF(N);]
%   The first column contains the ID in the SCF
%
%   The SCF is provided in ROW based, everything else should be organised
%   in column based
%
%   In 3D vectors the "z" dimension is the observation
%
%%% BUGS
%
%
%%% CHECK
%
%   Ratio
%   Check why there are repeated indexes!!!!!


close all;
clear variables;
addpath(genpath('../../functions'));

% PARAMETERS
plotmethod  = [ 0, 0, 0, 0, 0];   % chose what to plot, 1 - normaliser, 2 - ratio, 3 - diff
showFigures = 1;            % should figures be drawn
plotType    = 1;            % subplot or full plot
video       = 0;            % record video
showprb     = 0;
bw          = 20e6;
snrfactor   = 10*log10(bw);

% MCParameters
root        = './rfdata/';
fileset     = 'barcelona'; 
iter        = 50;
vCNR        = [80];
assert(root(end) == '/' || root(end) == '\');


nCNR        = length(vCNR);
interpSteps = 8000;
interpMax   = max(vCNR);
interpMin   = min(vCNR);
pcount      = 1;
figs        = createhandles(pcount,showFigures,plotType,1);
numHyp      = 4;

%ALGO PARAMETERS
detectionIDX = 1; % CHANGES DETECTION METHOD
szWindow     = 10;
wOffset      = 0;
previous     = 1e2;
factor       = 4; % choose as fs/factor


% FOR EACH CNR FILE
cfSEL     = [];
ready     = 0;
th        = 0.75;%[0:0.1:1];
figidx    = 0;
nbCases   = 1;
for W = 1:1
    cfset       = W;
    detectionTh = th;
    
    for T = 1:nCNR
        
        for inputtype=1:nbCases
%             switch inputtype
%                 case 1,%MIX
%                     binpath = [root,'hypData.',fileset,'#',num2str(iter),'$111@',num2str(vCNR(T)),'.mat'];
%                 case 2,%CMDA ONLY
%                     binpath = [root,'hypData.',fileset,'#',num2str(iter),'$101@',num2str(vCNR(T)),'.mat'];
%                 case 3,%OFDM ONLY
%                     binpath = [root,'hypData.',fileset,'#',num2str(iter),'$011@',num2str(vCNR(T)),'.mat'];
%                 case 0, %source input
%                     binpath = [root,'hypData.',fileset,'#',num2str(iter),'$001@',num2str(vCNR(T)),'.mat'];
%             end

%             binpath = [root,'hypData.barcelona#50$011@90D8.mat'];
            binpath = [root,'hypData.shch0100#1000#1.mat'];
%             binpath = [root,'hypData.stch0100#50#8.mat'];
            
            disp('ping')
            load(binpath);
            disp('pong')
            try
                sSt = sParam;
            catch
                sSt.stat = stat;
                sSt.sim  = sim;
                sSt.param = param;
                try
                    sSt.cyclo.alphaFAM = sSt.stat.alphas.*sSt.param.fref;
                catch
                    sSt.cyclo.alphaFAM = alphas.*sSt.param.fref;
                end
            end
            
            % sSt = stat
            scfSignal = sSt.stat.energySignal;
            scfNoise  = sSt.stat.energyNoise;
            alphas    = sSt.cyclo.alphaFAM./sSt.param.fref;
            nbObs     = size(scfSignal,1);
            
%             assert((size(scfSignal,1) ==  size(scfNoise,1)) && (size(scfSignal,2) ==  size(scfNoise,2)));
            
            switch inputtype
%                 case 5,%MIX
%                     mix      = scfSignal;
%                     mixNoise = scfNoise;
%                     psymbolCDMA(1,1) = sSt.sim.inputs(1,end)/sSt.param.fref;
%                     psymbolCDMA(1,2) = sSt.sim.inputs(1,end-1);
% 
%                 case 2,%CMDA ONLY
%                     cdma      = scfSignal;
%                     cdmaNoise = scfNoise;
%                     psymbolCDMA(1,1) = sSt.sim.inputs(1,end)/sSt.param.fref;
%                     psymbolCDMA(1,2) = sSt.sim.inputs(1,end-1);                    
%                     
                case 1,%OFDM ONLY
                    ofdm      = scfSignal;
                    ofdmNoise = scfNoise;
                    psymbolOFDM(1,1) = 1/sSt.sim.inputs(2,end)/sSt.param.fref;

            end
            
        end
        
        % DO NOT COMPUTE AFTER 1st run
        if ready == 0
            %COMPUTE ALL Cyclic Frequencies
            %CDMA the. cyclic frequencies
            xmin    = min(alphas);
            xmax    = max(alphas);
            psymbol = sSt.sim.inputs(1,end)/sSt.param.fref;
            xpos    = xmin:1:xmax;
            xpos    = xpos.*psymbol;
            xpos(xpos<xmin)=[];
            xpos(xpos>xmax)=[];
            cfCDMA  = xpos(:);
            CDMAfirstset = xpos(:);
            
            psymbol = sSt.sim.inputs(1,end-1);
            xpos    = xmin:1:xmax;
            xpos    = xpos.*psymbol;
            xpos(xpos<xmin) = [];
            xpos(xpos>xmax) = [];
            CDMAsecondset = xpos(:);
            
            % Keep all the unique CDMA frequencies
            cfCDMA  = uniqueFloatSafe([cfCDMA(:);xpos(:)]);
            
            % OFDM the. cyclic frequencies
            xmin    = min(alphas);
            xmax    = max(alphas);
            psymbol = 1/sSt.sim.inputs(2,end)/sSt.param.fref;%psymbolOFDM(1,1);%
            xpos    = xmin:psymbol:xmax;
%             xpos    = xpos.*psymbol;
            xpos(xpos<xmin)=[];
            xpos(xpos>xmax)=[];
            
            % Keep all the OFDM and merge them with CDMA frequencies
            cfOFDM = xpos(:);
            cfAll  = uniqueFloatSafe([cfCDMA;cfOFDM]);
            
            % EXCLUDE
            cfCDMA(cfCDMA == 0) = [];
            cfOFDM(cfOFDM == 0) = [];
            cfAll(cfAll == 0)   = [];
            
            cutoff = sSt.param.fs/(1e6*factor);
            
            cfCDMA(cfCDMA >cutoff) = [];
            cfOFDM(cfOFDM >cutoff) = [];
            cfAll(cfAll >cutoff)   = [];
            
            cfCDMA(cfCDMA <-cutoff) = [];
            cfOFDM(cfOFDM <-cutoff) = [];
            cfAll(cfAll <-cutoff)   = [];
            
            energyAll = zeros(length(cfAll),nbObs+1);
            [cfCommon,idxCommonOFDM,idxCommonCDMA] = intersect(round(cfOFDM*10000)/10000,round(cfCDMA*10000)/10000);
            
            cfMAP  = ones(length(cfAll),1);
            prbmap = zeros(length(cfAll),numHyp,nCNR);
            ready  = 1;
        end
        
        
        %PLOTS
        %             plot(sSt.cyclo.alphaFAM/sSt.param.fref,scfSignal(1,:), 'b-');hold on;
        %             % plot(alphas,scfSignal(1,:)./max(scfSignal(1,:))); hold on;
        %             stem(cfOFDM,0.15.*ones(length(cfOFDM),1),'x-r');
        %             stem(cfCDMA,0.15.*ones(length(cfCDMA),1),'o-y');
        
        for N=1:nbCases
            clear hitmap;
            figidx = 0;
            
            %Noise or signal
            for choice = 1 % PUT 0 back
                scfSW =[];
                scf =[];
                if choice == 1
                    hitidx = [1 2]; %SIGNAL
                    switch N
%                         case 1,%MIX
%                             scfSW = mix;
%                         case 2,%CMDA ONLY
%                             scfSW = cdma;
                        case 1,%OFDM ONLY
                            scfSW = ofdm;
                    end
                else
                    hitidx = [3 4]; %NOISE only
                    switch N
%                         case 1,%MIX
%                             scfSW = mixNoise;
%                         case 2,%CMDA ONLY
%                             scfSW = cdmaNoise;
                        case 1,%OFDM ONLY
                            scfSW = ofdmNoise;
                    end
                end
                
                
                
                
                %         %%% NOTE
                %         energy = energyAll;
                %         m=[];for k = 2:length(energy(:,1)), m(k)=(energy(k,1)-energy(k-1,1));end
                %         disp(['Mean CF distance: ',num2str(mean(m))]);
                %
                
                
                %Since now all the cf have been collected it is now time to decide if the
                %signal is present or not.
                
                % CREATE CF REGIONS
                % what to take into account
                %   Energy will contain IDX and SCF value
                %   cycFreq should be the corresponding SCF
                
                cfSEL    = [];
                deselect = [];
                % decide which region of cf to use
                switch cfset
                    
%                     case 1, % use !intersectoin(CDMA)
%                         deselect = ones(length(cfCDMA),1);
%                         deselect(idxCommonCDMA) = 0;
%                         cfSEL = cfCDMA(deselect==1);
%                         disp('using only CDMA subset');
                        
                    case 1, % use !intersectoin(OFDM)
                        deselect = ones(length(cfOFDM),1);
                        deselect(idxCommonOFDM) = 0;
                        cfSEL = cfOFDM;%(deselect==1);
                        disp('using only OFDM subset');
                        
                        %                     case 3, % use all cf
                        %                         cfSEL = cfAll; %THIS HAS TO RUN ALWAYS!
                        %
                        %                         %%%% FREE TO CHANGE ORDER AFTER CASE 1!
                        %                     case 45, % use all CDMA cf
                        %                         cfSEL = cfCDMA;
                        %                         cfSEL = [-22,-11,11,22];
                        %                         cfSEL  = cfCDMA(cfCDMA >0); % only use +
                        %                         cfSEL  = cfCDMA(cfCDMA <0); % only use -
                        %                         subsel = cfCDMA(cfCDMA >0);
                        %                         auxsel = [-22,-11,11,22];
                        %                         cfSEL  = uniqueFloatSafe([-subsel(1:5);subsel(1:5);auxsel(:)]); % only use +
                        %
                        %                     case 5, % use all OFDM cf
                        %                         cfSEL = cfOFDM;
                        %                         subsel = cfOFDM(cfOFDM >0);
                        %                         cfSEL  = [-subsel(1:5);subsel(1:5)];
                        %
                        %                     case 6, % use intersectoin(OFDM,CDMA)
                        %                         cfSEL = cfCommon;
                        %                         %
                        
                        
                    otherwise,
                        continue;
                end
                %
                
                % MARKS which frequencies are now being retrieved
                cfMAP = zeros(length(cfSEL),1);
                nbCF  = length(cfSEL);
                for k=1:nbCF
                    [~,idx]    = min(abs(cfAll-cfSEL(k)));
                    cfMAP(k,1) = idx;
                    %                 energy(k,:)  = energyAll(idx,:);
                end
                
                %Retrieve energy value from SCF
                energy = zeros(nbCF,nbObs+1);
                ngb = zeros(nbCF,6,nbObs);
                hitmap= zeros(nbCF,4,nbObs);
                for n=1:nbObs
                    
                    %select SCF observation
                    scf = scfSW(n,:);
                    
                    % Retrieve SCF value for every K CF
                    for k = 1:nbCF
                        if n ==1
                            [~,idx]     = min(abs(alphas-cfSEL(k)));
                            energy(k,1) = idx;                   % keep the idx
                            if k>1
                                assert(idx ~= energy(k-1,1),'Dups present'); % no repetitions
                            end
                        else
                            idx = energy(k,1);     % asume first observation indexes
                        end
                        energy(k,n+1)   = scf(idx);
                        phasecf(n,k)    = phase(scf(idx));
                        phasenoncf(n,k) = phase(scf(idx+100));
                        
                        % DO NEIGHBOUR SEARCH
                        %Check value next to each CF
                        ls  = scf(idx-wOffset-szWindow:idx-wOffset);  % Left/Down search
                        rs  = scf(idx+wOffset:idx+wOffset+szWindow);  % Right/Up search
                        fs  = scf(idx-wOffset-szWindow:idx+wOffset+szWindow); %full search
                     
                        m   = mean(fs);
                        s   = std(fs);
                        
                        ngb(k,1,n) = s/m;       % normaliser
                        ngb(k,2,n) = mean(ls);  % left neighbours mean
                        ngb(k,3,n) = mean(rs);  % right neighbours mean
                        ngb(k,4,n) = m;         % not tested in ratio
                        ngb(k,5,n) = mean(scf(idx-wOffset-szWindow:idx))/std(scf(idx-wOffset-szWindow-1:idx-1));
                        %                 ngb(k,5,n) = mean(scf(idx-offset-szWindow:idx))/std(scf(idx-offset-szWindow:idx));
                        ngb(k,6,n) = mean([previous,scf(idx-wOffset-szWindow:idx)])/std(scf(idx-wOffset-szWindow:idx));
                        previous   = mean([previous,scf(idx-wOffset-szWindow:idx)]);
                        
                    end % END OF FOR EACH CF
                    
                    % BUILD HITMAP for every CF
                    ph = ngb(:,detectionIDX,n)>=detectionTh;
                    nh = ngb(:,detectionIDX,n)<detectionTh;
                    try
                        assert(sum(ph) + sum(nh) == size(ngb(:,detectionIDX,n),1));
                    catch
                        error('Sherlock!!')
                    end
                    hitmap(ph,hitidx(1),n)=1; % Above th
                    hitmap(nh,hitidx(2),n)=1; % Below th
                    
                end % END OF EACH OBSERVATION
                
                %                 if choice ==1
                %                     plot(figs(1),cfSEL, ngb(:,1,n));
                %                 else
                %                     plot(figs(2),cfSEL, ngb(:,1,n));
                %                 end
                %                 if any(plotmethod)
                % %                     plotNormaliserMethods
                %                 end
                %
            end % END OF RUN FOR NOISE AND SIGNAL --- HITMAP CREATED
            
            % With the HITMAP constructed retrieve the Probability for EACH SET
            % FOR ALL OBSERVATIONS
            %   Compute probability
            prbcf = zeros(nbCF,4);
            for k=1:nbCF % cleared every N
                hit   = hitmap(k,1,:); %HIT
                miss  = hitmap(k,2,:); %MISS
                fhit  = hitmap(k,3,:); %False Positive
                fmiss = hitmap(k,4,:); %Good False
                prbmap(cfMAP(k),1,T) = sum(hit(1,:))/length(hit(1,:));
                prbmap(cfMAP(k),2,T) = sum(miss(1,:))/length(miss(1,:));
                prbmap(cfMAP(k),3,T) = sum(fhit(1,:))/length(fhit(1,:));
                prbmap(cfMAP(k),4,T) = sum(fmiss(1,:))/length(fmiss(1,:));
                disp(['#CF: ',num2str(cfSEL(k),'%06.2f'),...
                    ': T.POS ',num2str(sum(hit(1,:)),'%04d'),...
                    ': F.NEG ',num2str(sum(miss(1,:)),'%04d'),...
                    ': F.POS ',num2str(sum(fhit(1,:)),'%04d'),... %Noise HIT is Value < TH
                    ': T.NEG ',num2str(sum(fmiss(1,:)),'%04d'),...
                    ]);
                
                prbcf(k,1) = prbmap(cfMAP(k),1,T);
                prbcf(k,2) = prbmap(cfMAP(k),2,T);
                prbcf(k,3) = prbmap(cfMAP(k),3,T);
                prbcf(k,4) = prbmap(cfMAP(k),4,T);
                
                %save to xls
                assert(sum(hit(1,:))   + sum(miss(1,:)) == nbObs);
%                 assert(sum(fmiss(1,:)) + sum(fhit(1,:)) == nbObs);
            end
            
            % COMPUTE MEAN PROBABILITY over CF SET
            pd(N,T)   = mean(prbcf(:,1));
            pnd(N,T)  = mean(prbcf(:,2));
            fpd(N,T)  = mean(prbcf(:,3));
            fpnd(N,T) = mean(prbcf(:,4));
            
            %%% Or, should prob > th, then hit?
            
            % Probability of detection and cyclic frequency
            if showprb == 1
                figidx+1;
                stem(figs(figidx),cfSEL,prbmap(cfMap(:),1,T),'x');
                title(figs(figidx),['Probability of detection @Cyc. Freq ',num2str(vCNR(T))]);
            end
            
            % Tell me which CF have the biggest probability
            disp('-------------');
            disp(['STAT FOR CNR: ',num2str(vCNR(T))]);
            disp('TOP 5 - CF Probability ( CF | Pd )')
            
            clear sorted
            try
            sorted(:,1)      = cfSEL;
            sorted(:,2)      = prbmap(cfMAP,1,T);
            sorted           = sortrows(sorted(:,:),-2); % Sort by first row descending
            
            aboveZero        = sum(sorted(:,2) > 0);
            percFreq         = sum(sorted(:,2) > 0)/length(sorted(:,2));
            
            disp([sorted(1:5,1),sorted(1:5,2)]); % Disp top 10
            disp(['#CF>0  cyclic frequencies CONSIDERED: ',num2str(aboveZero)]);
            disp(['#TOTAL cyclic frequencies CONSIDERED: ',num2str(length(sorted(:,1)))]);
            disp(['%TOTAL cyclic frequencies CONSIDERED: ',num2str(percFreq*100)]);
            end
            thPD(N,T,W)  = pd(N,T);
            thFPD(N,T,W) = fpd(N,T);
            drawnow
        end % END of each CF SET
        
        
    end %end of each CNR file
    
end
%%
% PLOT PD over CNR for each set of CF ( ALL, CDMA, OFDM)

results = [{thPD},{thFPD}];
% publishplots;

disp(pd)

% if ~isempty(figs)
%     if pcount == 4
%         figidx = 0;
%     end
%     
%     snr = fix(vCNR-snrfactor);
%     
%     figidx = figidx+1;
%     plotPdCurves(figs(figidx),snr,pd,fpd,interpMin-snrfactor,interpMax-snrfactor,interpSteps,'Probability of Detection',nbCases);
%     
%     % figidx = figidx+1;
%     % plotPdCurves(figs(figidx),snr,pnd,interpMin-snrfactor,interpMax-snrfactor,interpSteps,'Probability miss detection (Signal)',nbCases);
%     %
%     % figidx = figidx+1;
%     % plotPdCurves(figs(figidx),snr,fpd,interpMin-snrfactor,interpMax-snrfactor,interpSteps,'Probability of false alarm (Noise)',nbCases);
%     %
%     % figidx = figidx+1;
%     % plotPdCurves(figs(figidx),snr,fpnd,interpMin-snrfactor,interpMax-snrfactor,interpSteps,'Probability of silent night (Noise)',nbCases);
%     
% end