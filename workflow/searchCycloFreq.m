function energy = searchCycloFreq(sSt, fax,scf,cycFreq,alphas)
    %SEARCHCYCLOFREQ obtains the cyclic frequency energy value
    %
    %   DESCRIPTION
    %   Looks in the provided SCF for the desired cyclic frequency
    %
    %   INPUT
    %   energy    - Array where to save energy values
    %   idx       - Array index
    %   fax       - frequency axis
    %   scf       - cyclic spectrum
    %   cycFreq   - cyclic frequencies to look for
    %   searchVal - values to compare to
    %
    %   OUTPUT
    %   energy    - Updated array
    %
    %   Pedro Silva
    %   Tampere University of Technology
    
    % Save value of specified cyclic frequency
    %     [~, f0FFT]  = min(abs(fax));
    %     searchspace = abs(scf(:,f0FFT));
    %
    %     for k=1:length(cycFreq)
    %         energy = searchspace(abs(searchVal - cycFreq(k)) < 0.001 - eps);
    %         %alphas/fref == cycFreq(k));
    %     end
    
    %     th       = 1.4;
    %     ngbdelta = 100;
    %     offset   = 50;
    
    % Returns scf
    [~, f0]     = min(abs(fax));
    %     f0=1;
    searchspace = scf(f0,:);
    energy      = searchspace;
    
    %     % Neighbours search
    %     %select SCF observation
    %     scf = scf(:,f0);
    %     nbCF = length(cycFreq);
    %     szWindow  = 10;
    %     energy = zeros(1,nbCF);
    %     % Retrieve SCF value for every K CF
    %     for k = 1:nbCF
    %
    %         [~,idx]     = min(abs(alphas-cycFreq(k)));
    %
    %         % DO NEIGHBOUR SEARCH
    %         fs          = scf(idx-szWindow:idx+szWindow); %full search
    %         m           = mean(fs);
    %         s           = std(fs);
    %         energy(1,k) = s/m;       % normaliser
    %
    %     end % END OF FOR EACH CF
    
end
