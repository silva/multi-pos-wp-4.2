function [txsignal,txnoise,cdma,ofdm] = genRFSignals(signals,inputs,fs,SNR,Hd)
    
    % Generate CDMA
    cdma = [];
    if signals(1,1)
        
        %(nbSamples, Nc, spFactor, modulation, modSelect, spCode, fc, fs)
        cdma = genCDMA(inputs(1,1),... % Number of samples
            inputs(1,2),... % Number of code Epochs
            inputs(1,3),... % Spreading factor
            inputs(1,4),... % Modulation
            inputs(1,5),... % Modulation selection QAM 1 PSK 0
            [],...
            inputs(1,6),... % Chip frequency
            fs);
    end
    
%     Nst,dF,pilotPw,modulation,modSelect,fs
    % Generate OFDM
    ofdm = [];
    if signals(2,1)
        ofdm   = genOFMD(inputs(2,1),... % Number of symbols
            inputs(2,2),...        % Total number of carriers
            inputs(2,3),...        % Number of DATA carriers
            inputs(2,6),...        % Number of Pilot and Data carriers
            inputs(2,4),...        % Carriers separation (1/dF)
            inputs(2,5),...        % Pilot power
            inputs(2,7),...        % Modulation
            inputs(2,8),...        % Modulation selection QAM 1 PSK 0
            fs);                   % Sampling frequency
    end
    
    % Prepare transmission signal
    PxOFDM = NaN;
    PxCDMA = NaN;
    if signals(1,1) && signals(2,1) % CDMA + OFDM
        lenCDMA = length(cdma);
        lenOFDM = length(ofdm);
        if(lenCDMA > lenOFDM)
            cdma(lenOFDM+1:end)=[];
        elseif (lenCDMA < lenOFDM)
            ofdm(lenCDMA+1:end)=[];
        end
        txsignal = cdma + ofdm;
%         txsignal = txsignal/max(abs(txsignal));%txsignal(:)./(sqrt(mean(abs(txsignal(:)).^2)));
        PxOFDM   = 10*log10(sum(abs(ofdm).^2)/length(ofdm));
        PxCDMA   = 10*log10(sum(abs(cdma).^2)/length(cdma));
        
    elseif signals(1,1) % CDMA only
        txsignal = cdma;
        PxCDMA   = 10*log10(sum(abs(cdma).^2)/length(cdma));
    elseif signals(2,1) % OFDM only
        txsignal = ofdm;
        PxOFDM   = 10*log10(sum(abs(ofdm).^2)/length(ofdm));
    else % ONLY noise
        error('Noise only no longer supported');
    end

    % SNR of the TX signal in dB
%     disp(['Variance CDMA: ', num2str(var(abs(cdma)))]);
%     disp(['Variance OFDM: ', num2str(var(abs(ofdm)))]);
%     disp(['Variance SIGNAL: ', num2str(var(abs(txsignal)))]);
    
    % noise std
    nr_samples = size(txsignal);
    noise_var  = 10.^(-SNR/20); % this is variance, hence / by 20. SNR in dB
    txnoise    = noise_var/sqrt(2)*(randn(nr_samples)+1i.^round(randn(nr_samples)));
    
%     PxTx = 10*log10(var(txsignal)/var(txnoise));
%     disp(['Tx Signal power: ', num2str(PxTx),' dB']);
    
    txsignal   = txsignal + txnoise;
    txsignal   = filter(Hd,txsignal); % Filter signal

    
%     pwelch(txsignal,[],[],[],'centered',fs)
end

%%EOF
%     if bf(1)==1 && af==1,
%         %do nothing
%     else   
%     %filter the signal with the filter designed above with B_T bandwidth and 0 delay 
%         txsignal = Fct_filterthesign_inpfilt(txsignal,af,bf,1);
%     end;