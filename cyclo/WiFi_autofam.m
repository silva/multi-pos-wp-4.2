                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%                        WiFi Auto FAM (After [11])                   % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
function [Sxp,fo,alphao,Np,centrow,centcol]=WiFi_autofam(famin,fs,df,dalpha,ss,index,v,shift)

if nargin~=8 
    error('Wrong number of arguments.'); 
end 
for ww=6:index*2 
    xin(1,(ss-16)*(ww-1)+1:(ss-16)*ww)=famin(1,ss*(ww-1)+16+1:ss*ww); 
end 
%define parameters 
Np=pow2(nextpow2(fs/df)); 
L=Np/4; 
P=pow2(nextpow2(fs/dalpha/L)); 
N=P*L; 
Sxp=zeros(Np+1,2*N+1); 
for w=1:1:index/2 
    x=xin(1,(shift)*(w-1)+1:(shift)*w);                     
       
   %input channalization 
if length(x)<N 
    x(N)=0; 
elseif length(x)>N 
    x=x(1:N); 
end 
NN=(P-1)*L+Np; 
xx=x; 
xx(NN)=0; 
xx=xx(:); 
X=zeros(Np,P); 
for k=0:P-1 
    X(:,k+1)=xx(k*L+1:k*L+Np); 
end 
  
%windowing 
  
a=hamming(Np); 
XW=diag(a)*X; 
%XW=X; 
  
%first FFT 
  
XF1=fft(XW); 
XF1=fftshift(XF1); 
XF=[XF1(:,P/2+1:P) XF1(:,1:P/2)]; 
  
%downconvertions 
E=zeros(Np,P); 
for k=-Np/2:Np/2-1 
    for k0=0:P-1 
        E(k+Np/2+1,k0+1)=exp(-i*2*pi*k*k0*L/Np); 
    end 
end 
XD=XF1.*E; 
XD=conj(XD'); 
  
%multiplication 
XM=zeros(P,Np^2); 


for k=1:Np 
    for c=1:Np 
        XM(:,(k-1)*Np+c)=(XD(:,k).*conj(XD(:,c))); 
    end 
end 
  
%second FFT 
XF2=fft(XM); 
XF2=fftshift(XF2); 
XF2=[XF2(:,Np^2/2+1:Np^2) XF2(:,1:Np^2/2)]; 
XF2=XF2(P/4:3*P/4,:); 
M=abs(XF2); 
alphao=-fs:fs/N:fs; 
fo=-fs/2:fs/Np:fs/2; 
Sx=zeros(Np+1,2*N+1); 
for k1=1:P/2+1 
    for k2=1:Np^2 
        if rem(k2,Np)==0 
            c=Np/2-1; 
        else 
            c=rem(k2,Np)-Np/2-1; 
        end 
        k=ceil(k2/Np)-Np/2-1; 
        p=k1-P/4-1; 
        alpha=(k-c)/Np+(p-1)/L/P; 
        f=(k+c)/2/Np; 
        if alpha<-1 | alpha>1 
            k2=k2+1; 
        elseif f<-.5 | f>.5 
            k2=k2+1; 
        else 
            kk=1+Np*(f+.5); 
            ll=1+N*(alpha+1); 
            Sx(round(kk),round(ll))=M(k1,k2); 
        end 
    end 
end 
Sxp=Sxp+Sx; 
end 
Sxp=Sxp./max(max(Sxp)); 
  
%    Surface Plot 
% Alpha=num2str(round(1+N*(alpha/fs+1)));  
% Fo=num2str(floor((Np+1)/2)+1); 
% V=num2str(v); 
% figure(4) 
% surfl(alphao,fo,Sxp); 
% view(-37.5,60); 
% xlabel('alpha'); 
% ylabel('f'); 
% zlabel('Sx'); 
% colormap jet 
% colorbar 
% axis([0 20*10^6 -9*10^6 9*10^6 0 1])
% 
% 
% %Contour Plot 
% figure(5) 
% contour(alphao,fo,Sxp,v); 
% xlabel('alpha,fo,Sxp'); 
% ylabel('f(Hz)'); 
% colormap winter 
% colorbar 
%   
%   
% %Cross-Section Plot 
% figure(6) 
% subplot(2,1,1), plot(alphao,Sxp(floor((Np+1)/2)+1,:)); 
% xlabel('alpha(Hz)'); 
% ylabel(['Sxp(',Fo,')']) 
% subplot(2,1,2), plot(fo,Sxp(:,round(1+N*(alpha/fs+1)))); 
% xlabel('f(Hz)'); 
% ylabel(['Sxp(',Alpha,')']); 
% centrow=floor((Np+1)/2)+1; 
% centcol=round(1+N*(alpha/fs+1)); 