function [a,J,alpha,r] = symrate_fft2(y,noncj,lags,alpha_res);
%
% function [a,J,alpha] = symrate_fft2(y,noncj,lags,alpha_res);
%
% Estimates the symbol rate of the given signal x using
% cyclic correlation. Returns the symbol rate divided by the
% sampling rate, i.e., a = f_sym/f_samp. Returns also the cost function
% J(alpha) and vector of cyclic frequencies alpha.
%
% y is the signal
% noncj is choice between testing for presence of conjugate or nonconjugate
%       cyclostationarity (1 if nonconjugate, and 0 if conjugate) [optional:
%       default = 0]
% lags is the vector of delays used to calculate cyclic correlations
%      [optional: default = -50:1:50]
% alpha_res is the upper bound for the step size used in cyclic frequencies
%           in practice step size next power of 2 of 1/alpha_res [optional:
%           default = 0.00001]
%
% [1] P. Ciblat, P. Loubaton, E. Serpedin, and G. B. Giannakis, "Asymptotic
% analysis of blind cyclic correlation-based symbol-rate estimators," IEEE
% Transactions on Information Theory, vol. 48, no. 7, pp. 1922-1934, July
% 2002. (W = I, i.e. W is identity matrix)
%
    
    if nargin < 4 | isempty(alpha_res),
        alpha_res = 0.00001;
    end;
    
    if nargin < 3 | isempty(lags),
        lags = -50:50;
    end;
    
    if nargin < 2 | isempty(noncj),
        noncj = 0;
    end;
    
    y = y(:).'; % Force y to a row vector
    N = length(y); % Number of samples
    
    lag_min = abs(min([0 min(lags)]));
    lag_max = max([0 max(lags)]);
    z = [zeros(1,lag_min) y zeros(1,lag_max)];
    
    y2 = zeros(length(lags),N); % Matrix for the autocorrelation coefficients
    
    % conjugate z if testing for nonconjugate cyclostationarity
    if noncj == 1,
        z = conj(z);
    end;
    
    % Calculate autocorrelation coefficients
    for k=1:length(lags),
        y2(k,:) = z(lags(k)+lag_min+1:end+lags(k)-lag_max).*y;
    end;
    
    % Calculate cyclic correlations at points alpha = k/Nfft (k=0,1,...,Nfft-1)
    % using FFT
    % Nfft = pow2(nextpow2(1/alpha_res));
    Nfft = max([pow2(nextpow2(1/alpha_res)),pow2(nextpow2(N))]);
    % Nfft = ceil(1/alpha_res);
    r = fft(y2,Nfft,2);
    alpha = (0:Nfft-1)/Nfft;
    
    J = zeros(size(r,2),1); % Utility function
    
    % Calculate utility
    for k=1:size(r,2),
        J(k) = sum(conj(r(:,k)).*r(:,k));
    end;
    
    % Find the highest peak
    [loc,val] = max(J);
    
    [m,m_ind] = min(val);
    a = alpha(m_ind(1));
end