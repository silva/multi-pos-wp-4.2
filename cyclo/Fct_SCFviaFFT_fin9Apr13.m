function [CAF, SCF, time_ax_CAF, fax_t]=Fct_SCFviaFFT_fin9Apr13(fs, alpha_lag,TxDataBit, window_type, FFTWin_length, Nblocks)
%fs is sampling frequency in samples, eg Ns*NBOC in CDMA
%alpha_lag is the vector of the cyclic frequencies of interest
%TxDataBit is the signal whose CAF we want to compute
%window_type is a window flag (1,2,or 3) stating whether we use any
%windowing or not. Option 1=no windo, option 2=Hanning window; option
%3=hamming window. The case 1 was tested, the others not so much
%FFTWin_length: size of FFT;
%Nblocks averaging interval
%input sequence should be > FFTWin_length*Nblocks, otherwise padding with 0
%see block diagram in: 
%http://www.google.fi/url?sa=t&rct=j&q=&esrc=s&source=web&cd=4&ved=0CGAQFjAD&url=http%3A%2F%2Fcost289.ee.hacettepe.edu.tr%2Fpublications%2FPortugal_Marques_01.ppt&ei=U


Ts=1/fs;
Len=length(TxDataBit);
Len1=FFTWin_length*Nblocks;
if Len1 < FFTWin_length*Nblocks,
    TxDataBit=[TxDataBit; zeros(Len1-Len-1)];
end;

time_ax=(0:Len1-1)'*Ts;
% time_ax=[0:Len1-1];
% Len_caf=min([FFTWin_length  length(TxDataBit)]);
% time_ax_CAF=[-Len_caf:Len_caf]*Ts;
 

switch window_type
    case 1
        Han_window=1;%ones(1,Len1);
    case 2
        Han_window=hanning(Len1);
    case 3
        Han_window=hamming(Len1);
end;

% Len=2*Len_caf+1;
% CAF=zeros(length(alpha_lag), Len);
SCF=zeros(length(alpha_lag),FFTWin_length);
CAF=zeros(length(alpha_lag),FFTWin_length);
%Txmatrix= (reshape(TxDataBit(1:Len1), FFTWin_Length, N_blocks));
%%FFTWin_length xN_blocks 

for  alph=1:length(alpha_lag),
%         expo_term_plus=exp(+j*pi*time_ax*alpha_lag(alph));
%         expo_term_minus=exp(-j*pi*time_ax*alpha_lag(alph));
        %cyclic autocorrelation function  is acf_caf
        s1=Han_window.*TxDataBit(1:Len1).*exp(+1i*pi*time_ax*alpha_lag(alph));
        s2=Han_window.*TxDataBit(1:Len1).*exp(-1i*pi*time_ax*alpha_lag(alph));
        Txmatrix1= (reshape(s1, FFTWin_length, Nblocks));
        Txmatrix2= (reshape(s2, FFTWin_length, Nblocks));
        FFT1=fftshift(fft(Txmatrix1));
        FFT2=fftshift(fft(Txmatrix2));
%         FFT1=(fft(Txmatrix1));
%         FFT2=(fft(Txmatrix2));
        CrossFFT=FFT1.*conj(FFT2); %matrix FFTWin_length xN_blocks 
        SCF(alph,:)=SCF(alph,:)+sum((CrossFFT).'); %summing after all Nblocks
    %do xcorr only on min([FFTWin_length  length(s1)]) lags
    CAF(alph,:)=fftshift(ifft(SCF(alph,:)));
end;
  
Len2=size(SCF,2); %FFTWin_length
fax_t=[-fs/2: fs/Len2: fs/2-fs/Len2];

% fax_t = time_ax;

time_ax_CAF=[-FFTWin_length/2:FFTWin_length/2-1];


SCF = SCF.';
CAF = CAF.';

end
