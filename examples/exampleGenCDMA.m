% Tampere University of Technology
%
% exampleGenCDMA.m
%
%   This script shows how to generate
%   cdma signals using the functions
%   developed. 
%
% --
% Pedro Figueiredo e Silva
% pedro.silva@tut.fi

close all;
clear all;
addpath(genpath('../functions'));

Ns = 4;
N  = 1000;

% Generates 1 Mbit/s signal
sg1mb = genCDMA(N,Ns,2^1,'psk'); % no channel coding

% Generates 2 Mbit/s signal
sg2mb = genCDMA(N,Ns,2^2,'psk');

% Print
figure;
plot(fftshift(abs(fft(sg1mb))));
title('1 Mbit/s 802.11 signal');

figure;
plot(fftshift(abs(fft(sg2mb))));
title('2 Mbit/s 802.11 signal');