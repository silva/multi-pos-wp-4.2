function [ cstr ] = numtocellstr( num )
    %NUMTOCELLSTR converts a numeric array to a cell str
    %   
    
    num = num(:);
    cstr = cellstr(num2str(num));
end

