%   Tampere University of Technology
%
%   RFLAB - Cyclostationarity
%
%%% DESCRIPTION:
%   This script controls the generation and analysis of several radio
%   signals features present in their cyclic autocorrelation function (CAF)
%   and spectrum autocorrelation function (SCF)
%
%%% ACKNOWLEGMENT:
%   Up to this moment, the functions that estimate these functions were
%   provided by Elena Simona, Tampere University of Technology.
%
%%% AUTHOR:
%   September 2013
%   Pedro Silva, Tampere University of Technology, 2013
%   pedro.silva@tut.fi
%
%%% NOTE!
%   steps in cyclic frequency and in frequency; normally delta_alpha
%   should be much smaller than delta_f
%
% Oversampling in PFDM is not done correcctly

% Clean up
clc;
close all;
clear variables;
addpath(genpath('./../../functions'));
addpath(genpath('./workflow'));

% Simulation
sSt   = getParameters();

% Receiver sampling
fsfactor   = sSt.sim.decFactor;
originalfs = sSt.param.fs;
change_cnr = 0;


% support multiple operation modes
for nRun = 1:sSt.sim.nbRuns
    
    
    % Restart counters
    if ~sSt.sim.signals(end)
        CNR          = sSt.sim.cnr(nRun);
        if change_cnr == 1
        CNR          = normrnd(CNR,10);
        end
        sSt.stat.cnr = CNR; % save current CNR value
        disp(['Run with CNR: ',num2str(CNR)]);
        fs =sSt.param.fs;
        sSt.param.deltaAlpha = sSt.param.dAlpha*fs; % Resolution in alpha domain
        sSt.param.deltaFreq  = sSt.param.dFreq*sSt.param.fs;    %
        sSt.param.alphaLags  = -fs/2:sSt.param.deltaAlpha:fs/2;  % Alphas axis
        sSt.param.limAlpha   = [-fs/2 +fs/2];          % Alpha limit
        sSt.param.limFreq    = [-fs/2 +fs/2];          % Frequency limit
    else
        disp(['Reading file: ',sSt.source.files(nRun,end-10:end)]);
        fs                   = sSt.source.fs(nRun);
        sSt.param.fs         = fs;
        sSt.param.deltaAlpha = sSt.param.dAlpha*sSt.param.fs; % Resolution in alpha domain
        sSt.param.deltaFreq  = sSt.param.dFreq*sSt.param.fs;    %
        sSt.param.alphaLags   = -fs/2:sSt.param.deltaAlpha:fs/2;  % Alphas axis
        sSt.param.limAlpha    = [-fs/2 +fs/2];          % Alpha limit
        sSt.param.limFreq     = [-fs/2 +fs/2];          % Frequency limit
        
        sParameters.stat.searchValue    = sSt.param.alphaLags/sSt.param.fref;
    end
    

    if nRun > 1
        sSt.stat.energySignal = [];
        sSt.stat.energyNoise  = [];
    end
    
    
    tic; % Main loop
    for runTime=1:sSt.sim.iterations
        % Generate Signals
        if ~sSt.sim.signals(end)
            [txsignal,txnoise,cdma,ofdm] = genRFSignals(sSt.sim.signals,sSt.sim.inputs,sSt.param.fs,CNR,sSt.sim.filter);
%             load('IEEEsimfs40.mat');
%             txsignal = total_td_sig(:);
%             txnoise=[];cdma=[];ofdm=total_td_sig(:);
        else % Read data from source
            [txsignal,txnoise,sSt.source.fid(nRun)] = retrieveSignals(sSt.source.fid(nRun),...
                sSt.source.files(nRun,:),...
                sSt.source.fs(nRun),...
                sSt.source.obsTime,...
                sSt.source.nbObs,...
                sSt.source.vth);
            cdma = [];
            ofdm = [];
        end
        
        
        % % % % % % % % % % % % % % % % % % %
        %TODO transmission/channel effects  %
        % % % % % % % % % % % % % % % % % % %
%         coord= 7.626e5:8.601e5;
%         txsignal = txsignal(coord);

        % INPUT decimation
        if sSt.sim.decimation
            txsignal = fftLowPass(txsignal, sSt.param.fs, originalfs/fsfactor*1/2,0,0);
% txsignal = fftLowPass(txsignal, sSt.param.fs, originalfs*3/4*1/2,0,0);
            txsignal = upsample(txsignal,3);
            txsignal = downsample(txsignal,4);
            sSt.param.fs = originalfs*3/4;
%             sSt.param.deltaAlpha = 0.0001*sSt.param.fs; % Resolution in alpha domain
%             sSt.param.deltaFreq  = 0.1*sSt.param.fs;    % Resolution in frequency domain
            
            fs                    = sSt.param.fs;

            sSt.param.deltaAlpha  = sSt.param.dAlpha*sSt.param.fs; % Resolution in alpha domain
            sSt.param.deltaFreq   = sSt.param.dFreq*sSt.param.fs;    % 
            sSt.param.alphaLags   = -fs/2:sSt.param.deltaAlpha:fs/2;  % Alphas axis
            sSt.param.limAlpha    = [-fs/2 +fs/2];          % Alpha limit
            sSt.param.limFreq     = [-fs/2 +fs/2];          % Frequency limit
            sParameters.stat.searchValue    = sSt.param.alphaLags/sSt.param.fref;
        end
        
        % Analyse spectrum
        if sSt.sim.exec(2)
            
            % Analyse signal
%             tic
            [scf,~,fax,alpha] = analyseSpectrum(sSt.sim.exec,sSt.param,txsignal);
%             toc
            sSt.cyclo.scf     = scf;
            sSt.cyclo.fax     = fax;
            sSt.cyclo.alpha   = alpha;
            
            % Search for cyclic frequencies
            cycFreq                          = sSt.stat.cycFreq;
            alphadomain                      = alpha./sSt.param.fref;
            sSt.stat.energySignal(runTime,:) = searchCycloFreq(sSt,fax,scf,cycFreq,alphadomain);

            
            % Analyse noise
            if sSt.sim.exec(3)
                [scf,~,fax,~]                   = analyseSpectrum(sSt.sim.exec,sSt.param,txnoise);
                sSt.stat.energyNoise(runTime,:) = searchCycloFreq(sSt,fax,scf,cycFreq,alphadomain);
                sSt.cyclo.scfNoise = scf;
                sSt.cyclo.fax      = fax;
                sSt.cyclo.alpha    = alpha;
            end
            
        elseif sSt.sim.exec(1) %TODO: RETEST - Energy detector
            [h0,h1] = energyDetector(txsignal,txnoise,sSt.param.fs);
            sSt.stat.energyNoise(runTime,1)  = h0;
            sSt.stat.energySignal(runTime,1) = h1;
        end
        
        % Plot results
        if any(sSt.sim.plot(1:end-3)) %faster
            plotSpectrum(sSt,runTime,cdma,ofdm,txsignal,txnoise);
        end
        
        % Alive indicator
        if ~mod(runTime,round(sSt.sim.iterations/10))
            if sSt.sim.splitSave
                savetodisk(sSt,sSt.sim.splitSave);
                sSt.sim.splitSave = sSt.sim.splitSave+1;
                disp(['(Saved) Iteration: ',num2str(runTime)]);
            else
                disp(['Iteration: ',num2str(runTime)]);
            end
        end
        
        % Capture frame for video
        if ~isempty(sSt.sim.video)
            captureframe(sSt.sim.video,sSt.sim.videoFig);
        end
        
    end
    
    if sSt.sim.exec(2)
        %Save alphas TODO: necessary?
        sSt.stat.alphas = alphadomain;
    end
    
    % Save runtime and data
    if sSt.sim.signals(end),
        for k = 1:length(sSt.source.fid)
            if ~isnan(sSt.source.fid(k))
                fclose(sSt.source.fid(k));
                sSt.source.fid(k) = NaN;
            end
        end
    end;
%     sSt.sim.runtime = toc;
    recordKeeping(sSt,nRun);
end


if ~isempty(sSt.sim.video)
    close(sSt.sim.video);
end

%exit;
%EOF%


