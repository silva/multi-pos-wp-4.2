# README #

This repository contains the source for the code developed under MULTI-POS WP4.2.

The master branch contains all the necessary code, while several individual branches contain more specific code, such as related to cyclostationary detection, plotting routines, generic tools, so on.

* Version
1.1 Added cyclostationary code and global functions