function symbols = cckmod(bitstream)

idx = 0;
    for k=1:4:(length(bitstream)-4)
        
        bits=bitstream(k:k+3);
        
        d01  = bin2dec(num2str(bits(1:2)));
%         d23 = bin2dec(num2str(bits(3:4)));
        d2 = bits(3);
        d3 = bits(4);
        
        phi1 =  getCCKPhase(d01,rem(k,2));
        phi2 = (d2 * pi) + pi/2;
        phi3 = 0;
        phi4 = d3* pi;
        
        idx = idx+1;
        symbol(:,idx) = [1*exp(1i*(phi1+phi2+phi3+phi4)),...
            1*exp(1i*(phi1+phi3+phi4)),...
            1*exp(1i*(phi1+phi2+phi4)),...
            -1*exp(1i*(phi1+phi4)),...
            1*exp(1i*(phi1+phi2+phi3)),...
            1*exp(1i*(phi1+phi3)),...
            -1*exp(1i*(phi1+phi2)),...
            1*exp(1i*(phi1))];
        
        if any(abs(symbol(:,idx)) == 0)
           disp(k); 
        end
        
    end
    
    symbols = symbol(:);
end



function ph = getCCKPhase(dbit,parity)


    phases = [...  %even        %odd
                    0,           pi;...
                    pi/2,        3*pi/2-pi/2;...
                    pi,          0;...
                    3*pi/2-pi/2, pi/2 ...
              ];

    ph = phases(dbit+1,parity+1);
end



