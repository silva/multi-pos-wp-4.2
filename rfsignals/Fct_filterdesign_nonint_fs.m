function [B_filter, A_filter,filter_order]= Fct_filterdesign_nonint_fs(filt_type,filter_parameters, ...
			   fsampling, B_T, bt_calc)
%design a filter with certain filter type and certain parameters;
%we can generate this filter at the beginning in the main file and use it inside the loops
%in the main file
%(=> optimization of the ressources)
%in here we don't compute or remove the filter delay
%INPUTS:
%
%filt_type =type of the filter (so far the following types may be
%           chosen: 'rect'=rectangular filter, 'fir' = FIR type
%           with Parks Algorithm, see help remez, 'cheb'=chebyshev IIR
%           type 1 filter, 
%           'butterworth' = Butterworh IIR filter)
%filter_parameters  = the parameters of the filter, given as vector
%                     with 3 elements, as follows
%[                    width_of_transition_band(in MHz)
%                     passband_ripple_in_dB/loss in passband [in
%                     dB]
%                     and attenuation in stopband [in dB]/ stopband_ripple_in_dB]
%fs                 = sampling rate in Hz
%B_T                = receiver bandwidth  in Hz (double sided=> positive bandwidth
%                     is B_T/2
%bt_calc =         a string equal either to 'passband', or to 'stopband'
%                  which tells us whether B_T is taken to be equal to the
%                  passband, to the stopband or to the 3-dB (or cutoff)
%                  bandwidth; default is 'passband'
%Len_sign_samples  = length of the signal that will be filtered with the
%                   filter designed here, at samples
%OUTPUTS:
%B_filter   = upper part of filter transfer function
%A_filter =lower part of filter transfer function
%delay_filter=delay introduced by the filter (estimated as the maximum of the ACF)
%created by Simona, 20 Jan 2005

if nargin<6,
  bt_calc='passband';
end;

%filter parameters:
%width of the transition band (in MHz)
tr_width=filter_parameters(1); 
rp=filter_parameters(2); %loss in passband
rs=filter_parameters(3); %attenuation in stopband
if strcmp(bt_calc, 'passband')
  fp=B_T/2;
  fs=fp+tr_width;
else
  if strcmp(bt_calc, 'stopband')
    fs=B_T/2;
    fp=fs-tr_width;
  else %3dB bandwidth (not very clear if it should be taken like this)
    fp=B_T/2-tr_width/2;
    fs=B_T/2+tr_width/2;
  end;
end;
Ts=1/fsampling;  %sampling time;

if strcmp(filt_type,'rect') 
  %windowed-sinc in time domain; rect
  %with ripples in freq domain
  %time pulse = windowed-sinc time pulse
  nr_coeff=10; %sinc number of coefficients (or half number); take
	       %it sufficiently high; e.g., around 10.
  time_ax=[-nr_coeff*Ts:Ts:nr_coeff*Ts];
  %here the cutoff and stopband frequencies are equal; therefore,
  %take them equal with the stopband of the other filters
  fcutoff=fs;
  %define the sinc function; this will be the denominator of the
  %time impulse response
  B_filter=sinc(time_ax*fcutoff);
  A_filter=1;
  filter_order=1; %assign some value for filter order; it has no
                  %meaning for RECT shapes
else 
  if strcmp(filt_type,'cheb') %chebyshev type 1 filter
    [N_cheb,W_cheb]=cheb1ord(fp/(fsampling/2),fs/(fsampling/2),rp,rs);
    [B_filter,A_filter]=cheby1(N_cheb,rp,W_cheb);
    filter_order=N_cheb;
  else
    if strcmp(filt_type,'butterworth')
      [N_butt, W_butt]=buttord(fp/(fsampling/2),fs/(fsampling/2),rp,rs);
      filter_order=N_butt;
      [B_filter,A_filter]=butter(N_butt,W_butt);
    else
      if strcmp(filt_type,'fir') 
        A_filter=[1 0]; %the desired amplitude in the bands A_filter
			%compute deviations, in linear scale, according to stpband
			%and passband ripples (according to Matlab help on web: 
			%http://www.mathworks.com/access/helpdesk/help/toolbox/signal/firpmord.html
       	dev = [(10^(rp/20)-1)/(10^(rp/20)+1)  10^(-rs/20)];
	
        [N_fir,F0,A0,W_fir]= firpmord([fp fs],A_filter,dev, fsampling);
        [B_filter,err]=remez(N_fir,F0,A0,W_fir);
        A_filter=1;
        %err
        while err>0.039
          N_fir=N_fir+1;  
	      [B_filter,err]=remez(N_fir,F0,A0,W_fir);
    	end;
	filter_order=N_fir;
    else
	error('Undefined filter type')
      end;
    end;
  end;
end;
