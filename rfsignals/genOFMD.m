function tx  = genOFMD(nbSym,nbCarriers,gi,pilotSpacing,pilotPw,BW,fs,modulation,modSelect,CNR)
    %GENOFMD generates an OFDM signal
    %   An OFDM signal is returned with thhe desired CNR and MODULATION.
    %   The signal is also oversampled by the OVERSAMPLE factor, which should
    %   be a positive number bigger than one.
    %   NBSAMPLES controls the size of the random generated binary message
    %   that is used throughout the process, if TXBITS is empty.
    %
    %   INPUT
    %   nbCarriers - Number of carriers (FFT size)
    %   overSample - Over sample factor
    %   modulation - Modulation size (>0 QAM, <0 DPSK)
    %   [modSelect]     - Modulation type ('psk' (by default) or 'qam')
    %   [sgPower]       - Desired power in Watts (default: normalised)
    %   [spreadingCode] - Use the specified spreading code
    %   [txBits]        - Message to be used (ignores nbSamples)
    %   [Ts]            - Sampling period (default is 20M samples/s)
    %
    %   OUTPUT
    %   tx         - OFDM signal
    %
    %   Pedro Silva, Tampere University of Technology, 2013
    
    %long training sequence
    % Ltrain = [1, 1, -1, -1, 1, 1, -1, 1, -1, 1, 1, 1, 1, 1, 1, -1, -1, 1, 1, ...
    %           -1, 1, -1, 1, 1, 1, 1, 0, 1, -1, -1, 1, 1, -1, 1, -1, 1, -1, ...
    %           -1, -1, -1, -1, 1, 1, -1, -1, 1, -1, 1, -1, 1, 1, 1, 1];
    % Strain = sqrt(13/6) * [0, 0, 1+1i, 0, 0, 0, -1-1i, 0, 0, 0, 1+1i,...
    %           0, 0, 0, -1-1i, 0, 0, 0, -1-1i, 0, 0, 0, 1+1i, 0, 0, 0, ...
    %           0, 0, 0, 0, -1-1i, 0, 0, 0, -1-1i, 0, 0, 0, 1+1i, 0, 0, ...
    %           0, 1+1i, 0, 0, 0, 1+1i, 0, 0, 0, 1+1i, 0,0];
    %
    % Input parameter assertion
    assert(nbCarriers > 0);
    assert(BW > 0);
    assert(modulation > 0);
    
    
    % Defaults
    if ~exist('CNR','var') || isempty(CNR)
        CNR = 1;
    else
        assert(CNR>0);
    end
    
    if ~exist('modSelect','var') || isempty(modSelect)
        modSelect = 'psk';
    end
    
    
    [E,F] = log2(nbCarriers);
    if E==0.5 %see help log2
        data = randsrc(nbCarriers,nbSym,0:modulation-1);
    else
        nbCarriers = 2^(F+1);
        data = randsrc(nbCarriers,nbSym,0:modulation-1);
    end
    
    % Encoding
    if modSelect == 1,
        mdata = qammod(data,modulation,[],'gray');
    elseif modSelect == 0,
        mdata = psk(data,modulation);
    end
    
    GI                          = nbCarriers/gi;
    dF                          = BW/nbCarriers;       % TFFT = 1/dF
    freqOffset                  = [-nbCarriers/2:-1 1:nbCarriers/2]'; % 0 not used
    symbol                      = zeros(nbCarriers,nbSym);
    Ts                          = 1/fs;
    
    % Pilot insertion
    if isnan(pilotSpacing)
        pilotPos                    = zeros(nbCarriers,1);
        pilotPos(freqOffset == -21) = 1;
        pilotPos(freqOffset == -7)  = 1;
        pilotPos(freqOffset == 7)   = 1;
        pilotPos(freqOffset == 21)  = 1;
   
        % serial for each symbol
    else
        pilotPos                            = zeros(nbCarriers,1);
        pilotPos(1:pilotSpacing:nbCarriers) = 1;
        pilotVal                            = ones(sum(pilotPos),nbSym).*2^(pilotPw);
        mdata(pilotPos==1,:)                = pilotVal;
    end
    
    k=0;kseq=0;
    header = []; body = []; tx = [];
    for n=1:nbSym
        k=k+1;
        if k < 11
            % Training sequence
            mdata(:,n) = trainSequence(1);
            % NO GI
            nogi=1;
        elseif  k < 13
            mdata(:,n) = trainSequence(0);
            nogi=0;
        else
            nogi=0;
            % Pilot signal
            mdata(pilotPos==1,n) = getPilotSignal(kseq).*(pilotPw);    
            mdata(32,n) = 0;
            mdata(1:6,n) = 0;
            mdata(end-4:end,n) = 0;
            kseq = mod(kseq+1,127);
        end
        
        % For all
        for t = 1:nbCarriers
            symbol(t,n) = sum(mdata(:,n).*exp(1i*2*pi*t.*freqOffset.*dF.*Ts));
        end

        %Insert GI in the non signal symbol
        if nogi==1
            header = [header; symbol(:,n)];
        else
            body   = [body; symbol(end-GI+1:end,n); symbol(:,n)];
        end
        
        
        if k == 19 
            k = 0;
            tx = [tx; header; body];
            header = []; body = [];
        end
        
    end
    
    %insert GI
%     symbol = [symbol(end-GI+1:end,:); symbol(:,:)];
%     tx     = symbol(:);
    
    if isempty(tx)
       tx = body(:); 
    end

    % Normalise signal power
    tx = tx./(sqrt(mean(abs(tx).^2)));
    tx = sqrt(10^(CNR/10)).*tx;
end


function data = psk(data,N)
    %PSK maps the input symbols to N chips
    %   USAGE
    %   data = psk(data,N)
    %
    %   INPUT
    %   data - symbol stream
    %   N    - constellation mapping size
    %
    %   OUTPUT
    %   data - BPSK or DQPSK modulated signal
    %
    %   Pedro Silva, Tampere university of Technology, 2013
    
    
    if N == 2 % QPSK
        data(data==0) = -1;
        
    elseif N == 4 % DQPSK
        data(data==0) = -1 - 1i;
        data(data==2) =  1 - 1i;
        data(data==1) = -1 + 1i;
        data(data==3) =  1 + 1i;
    else
        error('Available: BPSK and DPSK');
    end
    
end

%         tx(Td+1,1) = sum(mdata.*exp(1i*2*pi.*getFreqOffset().*dF.*(Td.*Ts-Tgi))) ...
%             + getPilotSignal(n).*sum(getPilot().*exp(1i.*2.*pi.*k.*dF.*(Td.*Ts-Tgi)));

function polarity = getPilotSignal(nbSymbol)
    %GETPILOTSIGNAL returns the pilot polarity for symbol N
    %
    %   USAGE
    %   signal = getPilotSignal(0) %for OFDM signal polarity
    %   singal = getPilotSingal(N) %for OFDM data polariity (N>0)
    %
    %   Notice that the first entry (0) is mapped to the first entry in the
    %   MATLAB vector (vec(1)).
    %
    %   !!!
    %   This entry is the OFDM SIGNAL polarity and to retrieve it, the sequence
    %   number should be used.
    %   !!!
    %
    %   The function uses N+1 to access the internal polarity vector
    %
    %   INPUT
    %   N        - Symbol sequence number
    %
    %   OUTPUT
    %   polarity - Polarity of pilot data for given symbol sequence number
    %
    %   Pedro Silva, Tampere University of Technology, 2013
    
    % Polarity of the pilot sequence
    p = [ 1, 1, 1, 1,-1,-1,-1, 1,...
        -1,-1,-1,-1, 1, 1,-1, 1,...
        -1,-1, 1, 1,-1, 1, 1,-1,...
        1, 1, 1, 1, 1, 1,-1, 1,...
        1, 1,-1, 1, 1,-1,-1, 1,...
        1, 1,-1, 1,-1,-1,-1, 1,...
        -1, 1,-1,-1, 1,-1,-1, 1,...
        1, 1, 1, 1,-1,-1, 1, 1,...
        -1,-1, 1,-1, 1,-1, 1, 1,...
        -1,-1,-1, 1, 1,-1,-1,-1,...
        -1, 1,-1,-1, 1,-1, 1, 1,...
        1, 1,-1, 1,-1, 1,-1, 1,...
        -1,-1,-1,-1,-1, 1,-1, 1,...
        1,-1, 1,-1, 1, 1, 1,-1,...
        -1, 1,-1,-1,-1, 1, 1, 1,...
        -1,-1,-1,-1,-1,-1,-1];
    
    if nbSymbol(end) <= length(p-1)
        polarity = p(nbSymbol+1);
    else
        polarity = repmat(p(2:end),1,round(nbSymbol(end)/length(p(2:end))));
    end
    
end



% 
function trainSeq = trainSequence(type)
    
    if type == 1
        tshort = sqrt(13/6).*[0, 0, 1+1i, 0, 0, 0, -1-1i,...
            0, 0, 0, 1+1i, 0, 0, 0, -1-1i,...
            0, 0, 0, -1-1i, 0, 0, 0, 1+1i,...
            0, 0, 0, 0, 0, 0, 0, -1-1i,...
            0, 0, 0, -1-1i, 0, 0, 0, 1+1i,...
            0, 0, 0, 1+1i, 0, 0, 0, 1+1i,...
            0, 0, 0, 1+1i, 0,0];
        trainSeq = tshort;
    else
        
        tlong = [1, 1, -1, -1, 1, 1, -1, 1, -1, 1, 1,...
            1, 1, 1, 1, -1, -1, 1, 1, -1, 1, -1,...
            1, 1, 1, 1, 0, 1, -1, -1, 1, 1, -1,...
            1, -1, 1, -1, -1, -1, -1, -1, 1, 1,...
            -1, -1, 1, -1, 1, -1, 1, 1, 1, 1];
        trainSeq = tlong;
    end
    
    trainSeq = [zeros(5,1);trainSeq';zeros(6,1)];
    
end
