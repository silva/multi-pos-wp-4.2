function [rx_filtered]= Fct_filterthesign_inpfilt(rx_signal,A_filter, ...
						  B_filter, delay_filter);   
%FUNCTION: filter the received signal rx_signal with a filter given as input,
%via the parameters B_filter, A_filter and delay_filter (these
%parameters are obtained beforehand with the function
%Fct_designfilter_opt.m
%otherwise, the functionality of this fct is the same as for
%Fct_filtered_sign_refcodeforIIR.m, but the advantge is that we can
%generate the filters only once, at the beginning, and we save time
%(no need for generating the filters at every random realization.

%('rect', 'remez', 'cheb',  'butterworth', 'fir'), 
%similar with Fct_filtered_sign_updt, but here we compute differently the
%delay for IIR and FIR filters: instead of taking the maximum value of the
%impulse response, we filter a reference code (without noise or Doppler,
%and we correlate this filtered reference code with the unfiltered ref
%code, and we find the position of the maximum peak (this will correspond
%to the delay introduced by the filter)
%INPUTS:
%rx_signal = received signal at sub-sample level (i.e., both BOC
%            modulation and oversampling are included)
%A_filter, B_filter, delay_filter  = the parameters of the filter,
%obtained via Fct_designfilter_opt.m
%OUTPUTS:
%rx_filtered = filtered signal
%created by Simona, 20 Jan 2005

%input signal power,
p1=mean(abs(rx_signal).^2);
%filtered signal, before delay removal
rx_filter_init=filter(B_filter,A_filter,rx_signal);
%filtered signal power
p2=mean(abs(rx_filter_init).^2);
rx_filt_norm=rx_filter_init*sqrt(p1/p2);
%filtered signal, after delay removal
rx_filtered=[rx_filt_norm(delay_filter:end-1); zeros(1,delay_filter)]; 
end
